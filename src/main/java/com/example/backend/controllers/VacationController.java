package com.example.backend.controllers;

import com.example.backend.models.Vacation;
import com.example.backend.services.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1")
public class VacationController {

    private final VacationService vacationService;

    @Autowired
    public VacationController(VacationService vacationService) {
        this.vacationService = vacationService;
    }

    @GetMapping("/vacations")
    public List<Vacation> getVacationsByCountry(@RequestParam Integer location_id, @RequestParam String start_date,
                                                @RequestParam String end_date, @RequestParam List<String> sports) {
        return vacationService.getSports(location_id, start_date, end_date, sports);
    }
}
